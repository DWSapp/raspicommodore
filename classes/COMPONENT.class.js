const Gpio = require('onoff').Gpio;

class COMPONENT {
    constructor( type, state, pin ){
        this.component = {
            type: type,
            state: state,
            pin: new Gpio(pin, type === 'LED' ? 'out' : 'in', 'both')
        };
        this.pin = pin;
    }

    switchState(state){
        state === 0 ? this.component.state = 1 : this.component.state = 0;
        this.component.pin.writeSync(this.component.state);
    }

    changeMouth(type){
        if( type === "smille" ){
            if( this.pin === 26 || this.pin === 20 || this.pin === 16 || this.pin === 6){
                this.component.pin.writeSync(1)
            }
            else{
                this.component.pin.writeSync(0)
            }
        }
        else if( type === "sad" ){
            if( this.pin === 21 || this.pin === 19 || this.pin === 13 || this.pin === 12){
                this.component.pin.writeSync(1)
            }
            else{
                this.component.pin.writeSync(0)
            }
        }
        else if( type === "kiss" ){
            if( this.pin === 19 || this.pin === 13 || this.pin === 20 || this.pin === 16){
                this.component.pin.writeSync(1)
            }
            else{
                this.component.pin.writeSync(0)
            }
        }
        else if( type === "error" ){
            if( this.pin === 21 || this.pin === 19 || this.pin === 16 || this.pin === 6){
                this.component.pin.writeSync(1)
            }
            else{
                this.component.pin.writeSync(0)
            }
        }
        else if( type === "close" ){
            this.component.pin.writeSync(0)
        }
    }
}

module.exports = COMPONENT