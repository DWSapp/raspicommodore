require('dotenv').config();
const express = require('express');
const path = require('path');
const Gpio = require('onoff').Gpio;



/* 
Server class
*/
class ServerClass{
  constructor(){
    // Components
    this.app = express();
    this.server = require('http').createServer(this.app);
    this.io = require('socket.io')(this.server);
    this.port = process.env.PORT;
  }


  init(){
    console.log(`ServerClass: init()`);
    this.app.set( 'view engine', 'ejs' );
    this.app.set( 'views', __dirname + '/www' );
    this.app.use( express.static(path.join(__dirname, 'www')) );
    

    this.setRouting();
  }

  setRouting(){
    console.log(`ServerClass: setRouting()`);
    this.app.get( '/', (req, res) => {
      res.render('index');
    });

    this.launch();
  }

  switchLedState( socket, data ){
    console.log('switchLedState()', data);

    // Emit led_state event
    socket.emit('led_state', { color: data, state: this.LEDS[data].state });
  }

  getButtonScate( socket ){
    for( let button of this.BUTTONS ){
      button.pin.watch( (err, value) => {
        if( err ){ console.error('There was an error', err) }
        else if( value === 1 ){
          if( button.state === 0 ){
            button.state = 1

            // Emit button_state event
            socket.emit('button_state', { type: button.name, state: button.state });
          }
          else{
            button.state = 0
            // Emit button_state event
            socket.emit('button_state', { type: button.name, state: button.state });
          }

          
        }
      });
    }
  }

  getRelayState(){
    this.RELAY.pin.writeSync(0)
    setTimeout(() => {
      this.RELAY.pin.writeSync(1)
      setTimeout(() => {
        this.RELAY.pin.writeSync(0)
      }, 2000);
    }, 2000);
  }

  setSocket(){
    console.log(`ServerClass: setSocket()`);

    this.io.on('connection', (socket) => {
      this.getButtonScate(socket);
      socket.on('switch_led', (data) => {
        this.switchLedState( socket, data )
      });
      
    });
  }

  launch(){
    console.log(`ServerClass: launch()`);

    this.server.listen(this.port, () => {
        console.log({
            node: `http://localhost:${this.port}`,
        });
    });
  };
}

/* 
Start server
*/
  const RASPIdwsapp = new ServerClass();
  RASPIdwsapp.init();
//
