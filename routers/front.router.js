/*
Importer les composants de la route
*/
const express = require('express');
const router = express.Router();
//


/*
Définition des routes
*/
// Afficher la liste des posts dans la page INDEX
router.get( '/', (req, res) => {
   
    res.render('index', { data: [
        { name: 'Julie,' },
        { name: 'Robert,' },
        { name: 'Peter,' },
    ], title: 'Hello from server' });
});
//

/*
Exporter le module de route
*/
module.exports = router;
//