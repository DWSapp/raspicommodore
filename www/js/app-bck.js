const getBtnClick = (tags) => {
  for( let btn of document.querySelectorAll(tags) ){
    btn.addEventListener('click', () => {
      socket.emit('switch_led', btn.getAttribute('data-led'));
    })
  }
}

const getRelayState = (tags) => {
  for( let input of document.querySelectorAll(tags) ){
    input.addEventListener('change', () => {
      socket.emit('switch_relay', { relay: input.getAttribute('data-relay'), value: input.checked ? 0 : 1 });
    })
  }
}


var socket = io();

// Get led_state event
socket.on('led_state', (data) => {
  console.log(data);
});

// Get button_state event
socket.on('button_state', (data) => {
  console.log(data);
})

// Get relay_state event
socket.on('relay_state', (data) => {
  console.log(data);
});

socket.emit('new connection', "Hello from Frontend");

document.addEventListener('DOMContentLoaded', () => {
  getBtnClick('.btn')
  getRelayState('.relay')
})