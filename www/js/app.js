const changeEye = (tags) => {
  for( let btn of document.querySelectorAll(tags) ){
    btn.addEventListener('click', () => {
      socket.emit('change_eye', btn.getAttribute('data-eye'));
    })
  }
}

const changeMouth = (tags) => {
  for( let btn of document.querySelectorAll(tags) ){
    btn.addEventListener('click', () => {
      socket.emit('change_mouth', btn.getAttribute('data-mouth'));
    })
  }
}

const changeExpression = (tags) => {
  for( let btn of document.querySelectorAll(tags) ){
    btn.addEventListener('click', () => {
      socket.emit('change_expression', btn.getAttribute('data-expression'));
    })
  }
}


var socket = io();

// Get eye_state event
socket.on('eye_state', (data) => {
  console.log(data);
})

// Get expression_state event
socket.on('expression_state', (data) => {
  console.log(data);
})

// Get mouth_state event
socket.on('mouth_state', (data) => {
  console.log(data);
})

document.addEventListener('DOMContentLoaded', () => {
  changeEye('.btn.eye');
  changeMouth('.btn.mouth');
  changeExpression('.btn.expression');
})