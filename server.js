require('dotenv').config();
const express = require('express');
const path = require('path');
const Gpio = require('onoff').Gpio;

const COMPONENT = require('./classes/COMPONENT.class')



/* 
Server class
*/
class ServerClass{
  constructor(){
    // Components
    this.app = express();
    this.server = require('http').createServer(this.app);
    this.io = require('socket.io')(this.server);
    this.port = process.env.PORT;
    this.eyes =  null;
    this.mouth =  null;
  }


  init(){
    console.log(`ServerClass: init()`);
    this.app.set( 'view engine', 'ejs' );
    this.app.set( 'views', __dirname + '/www' );
    this.app.use( express.static(path.join(__dirname, 'www')) );
    

    this.setRouting();
  }

  setRouting(){
    console.log(`ServerClass: setRouting()`);
    this.app.get( '/', (req, res) => {
      res.render('index');
    });

    this.launch();
  }

  setupHardware(){
    // Set Up eyes
    this.eyes = [
      [ new COMPONENT('LED', 0, 22), new COMPONENT('LED', 0, 23) ],
      [ new COMPONENT('LED', 0, 27), new COMPONENT('LED', 0, 24) ]
    ];

    // Set Up mouth
    this.mouth = [
      [ new COMPONENT('LED', 0, 26), new COMPONENT('LED', 0, 19), new COMPONENT('LED', 0, 13), new COMPONENT('LED', 0, 6) ],
      [ new COMPONENT('LED', 0, 21), new COMPONENT('LED', 0, 20), new COMPONENT('LED', 0, 16), new COMPONENT('LED', 0, 12) ]
    ];
  }

  changeEye( socket, type ){
    for( let LEDS of this.eyes ){
      for( let LED of LEDS ){
        if( type === "left" ){
          if( LED.pin === 22 || LED.pin === 23 ){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "left-med" ){
          if( LED.pin === 23 || LED.pin === 27 || LED.pin === 24){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "right" ){
          if( LED.pin === 27 || LED.pin === 24 ){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "right-med" ){
          if( LED.pin === 24 || LED.pin === 22 || LED.pin === 23){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "med" ){
          if( LED.pin === 23 || LED.pin === 24){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "open" ){
          LED.switchState(0);
        }
        else if( type === "shut" ){
          LED.switchState(1);
        }
      }
    }

    socket.emit('eye_state', type);
  }

  changeExpression( socket, type ){
    if( type === "wakup" ){
      // ox - oo
      setTimeout(() => {
        this.eyes[0][0].pin.switchState(0)
        // xx - oo
        setTimeout(() => {
          this.eyes[0][1].pin.switchState(0);
          // xx - ox
          setTimeout(() => {
            this.eyes[1][0].pin.switchState(0);
            // xx - xx
            setTimeout(() => {
              this.eyes[1][1].pin.switchState(0);
            }, 500);
          }, 500); 

        }, 500);
      }, 1000); // Wait 1s
    }

    socket.emit('expression_state', type);
  }

  changeMouth( socket, type ){
    for( let LEDS of this.mouth ){
      for( let LED of LEDS ){
        if( type === "joy" ){
          if( LED.pin === 26 || LED.pin === 20 || LED.pin === 16 || LED.pin === 6){
            LED.switchState(0)
          }
          else{  LED.switchState(1) };
        }
        else if( type === "sad" ){
          if( LED.pin === 21 || LED.pin === 19 || LED.pin === 13 || LED.pin === 12){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "kiss" ){
          if( LED.pin === 19 || LED.pin === 13 || LED.pin === 20 || LED.pin === 16){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "smille-left" ){
          if( LED.pin === 26 || LED.pin === 20 || LED.pin === 16 || LED.pin === 12){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "smille-right" ){
          if( LED.pin === 21 || LED.pin === 20 || LED.pin === 16 || LED.pin === 6){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "zig" ){
          if( LED.pin === 26 || LED.pin === 20 || LED.pin === 13 || LED.pin === 12){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "zag" ){
          if( LED.pin === 21 || LED.pin === 19 || LED.pin === 16 || LED.pin === 6){
            LED.switchState(0)
          }
          else{ LED.switchState(1) };
        }
        else if( type === "shut" ){
          LED.switchState(1);
        }
      }
    }
    socket.emit('mouth_state', type);
  }

  setSocket(){
    console.log(`ServerClass: setSocket()`);

    this.io.on('connection', (socket) => {

      socket.on('change_expression', (data) => {
        this.changeExpression(socket, data);
      });

      socket.on('change_eye', (data) => {
        this.changeEye(socket, data);
      });

      socket.on('change_mouth', (data) => {
        this.changeMouth(socket, data);
      });
    });
  }

  launch(){
    console.log(`ServerClass: launch()`);

    this.server.listen(this.port, () => {
        console.log({
            node: `http://localhost:${this.port}`,
        });
        this.setupHardware();
        this.setSocket();
    });
  };
}

/* 
Start server
*/
  const RASPIdwsapp = new ServerClass();
  RASPIdwsapp.init();
//
